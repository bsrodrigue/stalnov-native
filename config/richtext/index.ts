import FontFamilyStylesheet from "./stylesheet";
const fontFamily = 'EB Garamond';
const initialCSSText = { initialCSSText: `${FontFamilyStylesheet}`, contentCSSText: `font-family: ${fontFamily}` }

export default initialCSSText;